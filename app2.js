new Vue({
    el: '#demo',
    data: {
        tabs: { "tag1": false, "tag2": false, "tag3": false, "tag4": false, "other": false },
        selected: null, //默认没有选择tab，展示全部卡片
        cards: [
            {
                title: "cardA",
                description: "aaaa",
                keywords: ["tag1", "tag2"],
                image: "https://images.unsplash.com/photo-1575573550252-4bea31b9d485?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80",
            },
            {
                title: "cardB",
                description: "aaaa",
                keywords: ["tag3", "tag4"],
                image: "https://images.unsplash.com/photo-1575722719841-7770fb1aeaeb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
            },
            {
                title: "cardC",
                description: "aaaa",
                keywords: ["tag2"],
                image: "https://images.unsplash.com/photo-1575654001033-8befce082f06?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
            },
            {
                title: "cardD",
                description: "aaaa",
                keywords: ["tag1"],
                image: "https://images.unsplash.com/photo-1575687347857-a5687bb6e9ef?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
            },
            {
                title: "cardE",
                description: "aaaa",
                keywords: ["tag1", "tag4"],
                image: "https://images.unsplash.com/photo-1575695256604-adf031fac388?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
            },
            {
                title: "cardF",
                description: "aaaa",
                keywords: ["tag1", "tag2", "tag3"],
                image: "https://images.unsplash.com/photo-1575583966369-2f8ee5f77bd7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
            }
        ]
    },
    methods: {
        selectTab: function (tab) {
            // 设置selected。当前选择的tab
            if (this.selected == tab) {
                this.selected = null
            } else {
                this.selected = tab
            }
            // 更新tab列表的选择状态
            if (this.tabs[tab] == true) {
                this.tabs[tab] = false
            } else {
                for (t in this.tabs) {
                    if (t == tab) {
                        this.tabs[t] = true
                    } else {
                        this.tabs[t] = false
                    }
                }
            }
        }
    },
    computed: {
        // 计算需要显示的卡片，在index2.html模版中v-for渲染所有卡片使用的是此数据
        activeCards: function () {
            s = this.selected
            // filter参数为一个返回值类型为bool的函数，用于判断是否需要过滤掉这个数组的值
            return this.cards.filter(function (c) {
                // 若selected为空，即用户没有选中任何tab则全部显示
                if (s == null)
                    return true
                // 否则显示card keywords所匹配的
                return c.keywords.includes(s)
            })
        },
        // 是否显示全部卡片
        showAll: function () {
            var all = true
            for (t in this.tabs) {
                if (this.tabs[t]) {
                    all = false
                    break
                }
            }
            return all
        }
    }
});