// 判断元素是否在浏览器可视范围内
function isInViewport(elem) {
    var top_of_element = elem.offset().top;
    var bottom_of_element = elem.offset().top + elem.outerHeight();
    var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
    var top_of_screen = $(window).scrollTop();

    if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
        return true
    }
    return false
};

// 将指定selector匹配的元素加上class
function addClassForShow(selector, className) {
    $(selector).each(function (i, elem) {
        var e = $(elem);
        if (isInViewport(e)) {
            e.addClass(className);
        } else {
            e.removeClass(className)
        }
    });
}



